/*
	Mini Activity: 
		Create an expressjs API designated to port 4000
		Create a new route with endpoint /hello and method GET
			-Should be able to respond with  "Hello world"
*/

const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structure
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();

const port = 4000;

// Connect our server to database
mongoose.connect("mongodb+srv://admin:admin123@course-booking.wyhi8.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	});

// Set notifications for connection success or failure
// Connction to the database
// Allows us to handle errors when the initial connection is established
let db = mongoose.connection;

// If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection error"));

// If a connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Schemas determine the structure of the documents to be written in the database
// This acts as a blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// ACTIVITY #1 ---------------------------------------------- 

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

// ACTIVITY #1 ---------------------------------------------- 

// Model uses schemas and are used to create/instantiate objects that correspond to the schema
// Models must be in a singular form and capitalized.
// mongoose.model("nameOfCollectionInDatabase", fromWhereSchema)
// Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into plural form when creating a collection
const Task = mongoose.model("Task", taskSchema); 

// ACTIVITY #2 ---------------------------------------------- 

const User = mongoose.model("User", userSchema);

// ACTIVITY #2 ---------------------------------------------- 

// Setup for allowing the server to handle data from requests
// Allows our app to read json data
app.use(express.json());

// Allows our app to read data from forms
app.use(express.urlencoded({extended: true}));

// Creating a new task
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task tasks doesnt exist, we add it in our database
	2. The task data will be comming from the request body 
	3. Create a Task object with a "name" field/property

*/
app.post('/tasks', (req,res) => {

	//Check if there are duplicate tasks
	//If there are no matches, the value of result is null
	Task.findOne({name: req.body.name}, (err,result) => {

		//if a document was found and the document's name matches the information sent via the client/Postman
		if (result != null && result.name == req.body.name) {

			//Return a message to the client/postman
			return res.send("Duplicate task found");
		
		//if no document was found	
		} else {

			//Create a new Task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			//save method will store the information to the database
			newTask.save((saveErr, savedTask) => {

				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)

				//no error found while creating the document
				} else {

					//returns a status code of 201 and sends a message "New Task created."
					return res.status(201).send("New Task created")
				}
			})
		}
	});
}); 


// ACTIVITY #3 & #4 ---------------------------------------------- 
app.post('/signup', (req,res) => {
	if (req.body.username != '' && req.body.password !='') {
		
		let newUser = new User ({
			username: req.body.username,
			password: req.body.password
		});

		newUser.save((saveErr, savedTask) => {
			if(saveErr) {
				return console.error(saveErr)
			} else {
				return res.status(201).send("New user registered")
			}
		})
		
	} else {
		res.send(`Please input both username and password`);
	}
})
// ACTIVITY #3 & #4 ---------------------------------------------- 


// GET request to retrieve all the documents
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success back to the client
*/
app.get('/tasks', (req,res) => {
	
	Task.find({}, (err,result) => {
		
		if (err) {
			console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})



app.get('/hello', (req,res) => {
	res.send(`Hello world`);
});

app.listen(port, () => console.log(`server is running at port ${port}`));